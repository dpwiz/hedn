---
title: Banana schemes
---

```haskell top hide
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

import Data.Text (Text)
import Text.Show.Pretty (ppShow)
import Data.Functor.Foldable (Fix(..), Recursive(project, cata, para), fold)

import qualified Data.Text as Text

import Data.EDN
import Data.EDN.QQ (edn)
import Data.Functor.Base.EDN (ValueF(..))

pretty :: Show a => a -> Text
pretty = Text.pack . ppShow
```

# TL;DR

`Value` is neither `Foldable`, nor `Functor`.

Since there's no type variable to vary about, `Value` can't provide
a list-like spine to carry intermediate (co-)recursion data.

An adapter for [recursion schemes] *fixes* that.

[recursion schemes]: https://hackage.haskell.org/package/recursion-schemes

# How?

From the `recursion-schemes` README:

> The idea is that a recursive function may be broken into two parts:
> the part which is the same in all the recursive functions which follow
> a given recursion scheme, and the part which is different in each function.

So, whatever the job, it is half-done already!

Folding is quite straight-forward with *inductive* types like lists.

```haskell top
numbers :: [Int]
numbers = 1 : 2 : 3 : 4 : 5 : []

sumList :: [Int] -> Int
sumList = foldr (+) 0
```

The thing is, trees are not much different.

```haskell top
data Tree a = Node a [Tree a]
  deriving (Show, Foldable)

tree :: Tree Int
tree =
  Node 1
    [ Node 2 []
    , Node 3
        [ Node 4 []
        , Node 5 []
        ]
    ]

sumTree = foldr (+) 0 :: Tree Int -> Int
```

Wait, what? The implementation is exactly the same.
Why? Because exact data structure is hidden from the `fold` function.
It only gets a stream of elements to consume and feeds them into your function
(`(+)` in this example) one by one.

Notice the family resemblance:

```haskell eval
pretty numbers
```

```haskell eval
pretty tree
```

What about EDN then? Can

```haskell top
expr :: TaggedValue
expr = [edn|
  (1
    (2 3)
    (4 5))
|]

sumEDN :: TaggedValue -> Int
sumEDN tv =
  case stripTag tv of
    Integer i ->
      i
    List xs ->
      sumList (map sumEDN xs)
    _ ->
      -- TODO
      0
```

The exact layout does not matter as long as the intersting values are the same.

```haskell eval
pretty expr
```

```haskell eval
pretty $ sumEDN expr
```

Can you just use the fold? Nope. Why?

```haskell
class Foldable (t :: Type -> Type) where
  foldr :: (a -> b -> b) -> b -> t a -> b
```

It's type says the type of the elements can be derived from the parameter
of a container.
While lists clearly state the type of their elements, `TaggedValue` has
all sorts of stuff inside.

The folding function would have to accept then a whole value at each step
and could potentially inspect its inner details while having to restrain
from counting each element multiple times.

What would be its type?

```haskell top
type FoldEDNunsafe acc =
     TaggedValueFun acc
  -> acc
  -> TaggedValue
  -> acc

type TaggedValueFun acc = TaggedValue -> acc -> acc
```

> Note: The function bodies are omited for brevity but the types are checked
> and will guide the implementation.

Due to mutually-recursive nature of `Tagged Text Value` and `Value` it
will be a bit of a hassle to extract the value from both constructors of
the `Tagged` type.
And since it is known that *every* value is wrapped, it can be extracted
and put alongside.

```haskell top
type TextTag = Maybe (Text, Text)

type TagAndValueFun acc = TextTag -> Value -> acc -> acc
```

Alternatively, there can be another function that will handle all the recursion
but require as many arguments as there are different constructors in the type.

```haskell top
type FoldEDN acc =
     NilFun acc
  -> IntegerFun acc
  -> ListFun acc
  -> SetFun acc
  -> MapFun acc acc
  -> acc
  -> TaggedValue
  -> acc

-- | Nil has no values inside, but we may to peek at its tag.
type NilFun acc = TextTag -> acc -> acc

-- | Integer has the Int, wrapped.
type IntegerFun acc = TextTag -> Int -> acc -> acc

-- | Container types are tricky.
--   Let's allow to peek at the labels and access values collected so far,
--   but not the contents.
type ListFun acc = TextTag -> [acc] -> acc -> acc

-- | Non-functor constructors are even trickier.
--   We cannot simply put accumulated values into a Set since there may be
--   no Ord instance for the accumulator.
type SetFun acc = TextTag -> [acc] -> acc -> acc

-- | Map is the trickiest of them all.
--   EDN maps can have any expression as a key and need to be handled
--   with extra care.
--   For now, let's skip over it and put a separate accumulator for tkeys.
type MapFun keyAcc valAcc =
     (TextTag, keyAcc)
  -> (TextTag, valAcc)
  -> valAcc
  -> valAcc
```

Can we have a convenience of having a single function (with a nice
`LambdaCase` in the body) with safety of a you-only-get-the-accumulator fold?

```haskell top
sumEDN2 :: TaggedValue -> Int
sumEDN2 = foldEDNInt f 0
  where
    f acc _tag = \case
      Integer i -> i + acc
      List tvs  -> sum (map sumEDN2 tvs) -- XXX: uh-oh, where's fold in that?
      _         -> 0

    foldEDNInt :: (a -> TextTag -> Value -> a) -> a -> TaggedValue -> a
    foldEDNInt fun tv acc = undefined
```

The problem is, for `List` to be capable of type-changing maps and folds
it has to accept any type and not a single concrete one.
And so do other structural constructors.
We have to remove `TaggedValue` from itself!

```haskell top
data NonrecValue a
  = IntLeaf TextTag Int
  | ListVals TextTag [a]
  deriving (Foldable)
```

We can even derive `Functor` and `Foldable` for it.
Too bad we can't put it in iself anymore as it would require having an infinte type:

```haskell
NonrecValue (NonrecValue (NonrecValue (NonrecValue (.. -- never ends
```

## Recursive

But wait, there's a Hackage library exactly for that!

<https://hackage.haskell.org/package/recursion-schemes>

It contains all the bits necessary to construct such types and means
to consume values for them.

To get access to all this fancy machinery with even fancier names there
should be "base" type like `NonrecValue` and it must be an instance
of `Functor` class.

> You can try deriving the base functor for simple types with Template Haskell
> but that way is not available for TaggedValue due to multiple levels of
> recursion and non-functor containers.

```haskell
data ValueF f
  = NilF       TextTag
  | BooleanF   TextTag Bool
  | StringF    TextTag Text
  | CharacterF TextTag Char
  | SymbolF    TextTag Text Text
  | KeywordF   TextTag Text
  | IntegerF   TextTag Int
  | FloatingF  TextTag Double
  | ListF      TextTag [f]
  | VecF       TextTag (V.Vector f)
  | MapF       TextTag [(f, f)]
  | SetF       TextTag [f]
  deriving (Eq, Show, Generic, Functor, Foldable, Traversable)
```

With `Fix` and `Functor` instance you can construct recursive values
from non-recursive types.

```haskell top
numbersF :: Fix ValueF
numbersF =
  Fix (ListF (Just ("a", "tag"))
    [ Fix (IntegerF Nothing 1)
    , Fix (ListF Nothing
        [ Fix (IntegerF Nothing 2)
        , Fix (IntegerF Nothing 3)
        ]
      )
    , Fix (ListF Nothing
        [ Fix (IntegerF Nothing 4)
        , Fix (IntegerF Nothing 5)
        ]
      )
    ]
  )
```

What a nice little pile of boilerplate!
Fortunately, you don't have to construct it yourself if you already have
a `TaggedValue` at hand.

The `Recursive` type class from `Data.Functor.Foldable` has a
handy method named `project` that converts a single layer of original type
to its base functor.
It would be used automatically if you apply a "recursion scheme".

The simplest of them probably is "just leave the projected value as it were".
Because schemes remove

```haskell top
projectDeep :: TaggedValue -> Fix ValueF
projectDeep = fold Fix
```

### Display

For something more involved consider a fold where its accumulator
is a text representation of the elements a current value contains:

```haskell top
display :: TaggedValue -> Text
display = cata displayF

displayF :: ValueF Text -> Text
displayF = \case
  NilF Nothing        -> "nil"
  NilF (Just ("", n)) -> "#" <> n <> " nil"
  NilF (Just (ns, n)) -> "#" <> ns <> "/" <> n <> " nil"

  ListF _t xs         -> "(" <> Text.unwords xs <> ")"

  _ -> error "TODO"

nillyWilly :: TaggedValue
nillyWilly = [edn| (nil #foo/bar nil) |]
```

```haskell eval
display nillyWilly
```

### Eval

> A little-known obvious fact that you can fold to a value of any type,
> not just "simple" things like `()`.
> `IO ()` has values too.
> With usual value binding procedures you can access the value inside and
> pass another one downstream.

Consider this single-operation expression-checking calculator:

```haskell top
arith
  :: (Double -> Double -> Double)
  -> Double
  -> TaggedValue
  -> Either Text Double
arith op neutral = cata $ \case
  IntegerF _t i ->
    Right (fromIntegral i)

  FloatingF _tag d ->
    Right d

  ListF _tag inner -> do
    -- Either is a monad, after all.
    accs <- sequence inner
    Right (foldr op neutral accs)

  NilF _tag ->
    Left "nil is not a number"

  _ ->
    undefined
```

```haskell eval
pretty (arith (+) 0.0 expr)
```

```haskell eval
pretty (arith (*) 1.0 nillyWilly)
```

While it's a good thing we can ask it to calculate with *our* functions
and it's even "upgrades" integers to a common type it can do more.

For example it can detect the function and parameters by itself when lists
are starting with an operation symbol.

There's a problem that then the action comes to `ListF`, all of its elements
are already crunched into numbers.
Luckily there is another recursion scheme function that presents the original
values together with accumulated result: `para`.

```haskell top
calc :: TaggedValue -> Either Text Double
calc = para $ \case
  SymbolF _t _ns _op ->
    -- A symbol standing by itself has no numeric value.
    Left "Operator in a value position"

  IntegerF _t i ->
    Right (fromIntegral i)

  FloatingF _tag d ->
    Right d

  ListF _tag inner ->
    case inner of
      -- As expected, the list starts with a symbol.
      -- It's allright for it to have an "error" attached...
      (NoTag (Symbol _ns op), Left _) : args -> do
        -- Ignore the original values, but they have to be numbers.
        checked <- sequence (map snd args)
        case op of
          "+" ->
            Right (foldr (+) 0.0 checked)
          "-" ->
            Right (foldr (-) 0.0 checked)
          "*" ->
            Right (foldr (*) 1.0 checked)
          "/" ->
            -- From the clojure docs:
            -- If no denominators are supplied, returns 1/numerator,
            -- else returns numerator divided by all of the denominators.
            case checked of
              []       -> Left "No arguments given to `/`"
              [n]      -> Right (1 / n)
              (n : ds) -> Right (foldl (/) n ds)
          _ ->
            Left $ "Unknown operation: " <> op
      (tv, _) : _args ->
        Left $ "Form head is not a symbol: " <> display tv

  NilF _tag ->
    Left "nil is not a number"

  _ ->
    undefined
```

```haskell eval
pretty $ calc [edn|
  (+
    (/ 6 3 2)
    (* 2 3)
    (- 4 5))
|]
```

Now implement the rest of [clojure.core](https://clojuredocs.org/clojure.core).
