---
title: AST in EDN and Haskell
---

# EDN

From the EDN [specs](https://github.com/edn-format/edn) page:

> **edn** supports a rich set of built-in elements, and the definition of extension elements in terms of the others.
> Users of data formats without such facilities must rely on either convention or context to convey elements not included in the base set.
> This greatly complicates application logic, betraying the apparent simplicity of the format.
> **edn** is simple, yet powerful enough to meet the demands of applications without convention or complex context-sensitive logic.

# Haskell

In haskell this "rich set of built-in elements" is represented with two types.

A container:

```haskell
data Value
  = Nil
  | Boolean   Bool
  | String    Text
  | Character Char
  | Symbol    Text Text
  | Keyword   Text
  | Integer   Int
  | Floating  Double
  | List      EDNList
  | Vec       EDNVec
  | Map       EDNMap
  | Set       EDNSet
```

And a generic wrapper for tagging things:

```haskell
data Tagged tag a
  = Tagged tag tag a
  | NoTag a
```

Then, they are combined in a resulting container:

```haskell
type TaggedValue = Tagged Text Value
```

# Parsing

To convert between text representation and AST there are two aptly named functions (collected in `Data.EDN`):

```haskell
parseText :: Monad m => String -> Text -> m TaggedValue
renderText :: TaggedValue -> Text
```

```haskell top hide
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.IO as Text
import Text.Show.Pretty (ppShow)

import Data.EDN

pretty :: Show a => a -> Text
pretty = Text.pack . ppShow
```

```haskell eval
pretty $ parseText "example" "#fancy/s-expressions (are [my/bread and butter])"
```

```haskell eval
renderText $ NoTag (Symbol "my" "bread")
```

> Note: `parseText` expects its input as a signe-expression document.
> You need to wrap it in your favourite container to have "multiple declarations".

While pattern-matching on this simple if "rich" set of constructors is fun,
there is a way to shuttle values to and from EDN documents.
