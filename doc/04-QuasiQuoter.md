---
title: DSLs made easy
---

With `FromEDN` instances it is time for EDN's "extensible" part to shine.

With Haskell `QuasiQuotes` extension you can write out EDN values in their
native syntax.

```haskell top hide
{-# LANGUAGE QuasiQuotes #-}

import Data.Text (Text)
import Text.Show.Pretty (ppShow)

import qualified Data.Text as Text

import Data.EDN
import Data.EDN.QQ

pretty :: Show a => a -> Text
pretty = Text.pack . ppShow
```

```haskell top
fredEDN :: TaggedValue
fredEDN = [edn|
  #myapp/Person {:name "Fred Mertz" :title "Mx"}
|]
```

```haskell eval
pretty fredEDN
```

```haskell eval
renderText fredEDN
```

Sometimes it is more convenient to have a collection of elements,
but omit the collection wrapper symbols.

There are specialized quasiquoters for that.

> Note: the resulting type is `Value`, and you may need to
> prepend it with `Tagged`/`NoTag`.

**Lists**, `()`:

```haskell eval
renderText $ NoTag [ednList|
  this is a list of symbols, commas are whitespace
|]
```

**Vectors**, `[]`:
```haskell eval
renderText $ NoTag [ednVec|
  42 is #the/answer true
|]
```

**Sets**, `#{}`
```haskell eval
renderText $ NoTag [ednSet|
  badger badger badger badger
  badger badger badger badger
  mushroom mushroom
|]
```

> Note: yes, sets will be normalized to their "unique elements" form
> and no duplicate elements will exist in runtime.

```haskell eval
pretty [ednSet| a is a |]
```

**Maps**, `{}`
```haskell eval
renderText $ NoTag [ednMap|
  :keywords "as you like it"
  or/symbols "I won't judge"
  #uuid "d748ab62-9cb1-41fb-b8dc-e23f3ffc5f9b"
  (tagged values, collections, anything goes)
|]
```

> Note: the map keys are ordered wrt. their Haskell constructors and, then, contents.

It is possible to generate EDN quasiquoters for your values
by specializing `fromEDN` QuasiQuoter.

> Usual TH shenanigans apply.
> Define things in a separate module.
> Either `Data.Data.Data` or `Language.Haskell.TH.Syntax.Lift` instance required to convert values to Haskell AST.

```haskell
module My.Data.QQ (myData) where

import Data.EDN.QQ (fromEDN)
import My.Data.Types (MyData)

myData :: QuasiQuoter
myData = fromEDN @MyData
```

```haskell
module Main where

import My.Data.QQ (myData)

main :: IO ()
main = cobc
  [myData|
    {:identification-division
        [#program/id hello]
     :procedure-division
        ((display "Hello, world!"
          end-display)
         stop-run
        )
    }
  |]
```
