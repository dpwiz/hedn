---
title: Parsing the parsed
---

```haskell top hide
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

import Data.Text (Text)
import Data.EDN
  ( TaggedValue
  , Tagged(..)
  , Value(..)
  , mkMap
  , FromEDN(..)
  , fromEDN
  , withTagged
  )

import qualified Data.Text as Text
import qualified Data.Map.Strict as Map

import Text.Show.Pretty (ppShow)

import Data.EDN
import Data.EDN.Class.Parser (Parser, parseEither)

pretty :: Show a => a -> Text
pretty = Text.pack . ppShow

data Person = Person
  { pName  :: Text
  , pTitle :: Text
  } deriving (Eq, Show)
```

Decoding the EDN AST can be tedious and tricky.
Let's first take a look at what happens under the hood.

```haskell top
decodePerson :: TaggedValue -> Maybe Person
decodePerson tv =
  case tv of
    Tagged "myapp" "Person" v ->
      case v of
        Map m -> do
          tvName <- Map.lookup (NoTag $ Keyword "name") m
          tvTitle <- Map.lookup (NoTag $ Keyword "title") m
          case (tvName, tvTitle) of
            (NoTag (String name), NoTag (String title)) ->
              Just Person
                { pName = name
                , pTitle = title
                }
    Tagged _tagNS _tag _v ->
      -- Wrong tag
      Nothing
    NoTag _v ->
      -- Even more wrong!
      Nothing

ednFred :: TaggedValue
ednFred =
  Tagged "myapp" "Person" $ mkMap
    [ (NoTag $ Keyword "name", NoTag $ String "Fred")
    , (NoTag $ Keyword "title", NoTag $ String "Mx")
    ]
```

```haskell eval
pretty $ decodePerson ednFred
```

> Note: take a minute to meditate on how `Monad` instance of `Maybe a` helps
> to shave a few layers of `case`.

What about `PersonRow`?
Write more pattern-matching and combinators?
Let's not.

Enter `Data.EDN.Class.Parser` ~~so you can parse while you parse~~.

This little one distills an essense of case-cading pattern matching to
a classic trio of `Functor`, `Applicative` and `Monad`
(and `Alternative`, more on that ater).
The type itself can look opaque, hopefully you don't need to deal with
it directly.
Neither you need to unwrap EDN constructors (most of the time).

There are set of helpers in `Data.EDN.Class` that take a value and produce
either an internal value or terminates with an error message.

First things first, we need to check the tag.

```haskell top
personParser0 :: TaggedValue -> Parser Person
personParser0 =
  withTagged "myapp" "Person" $ \_v ->
    pure $ Person "TODO" "Later"
```

It will reject the incorrectly labeled values so you don't have to
invent a clever way to mock user with error messages.

```haskell eval
pretty $ parseEither personParser0 ednFred
```

```haskell eval
pretty $ parseEither personParser0 (NoTag Nil)
```

Next we have to decide what kinds of EDN elements we want to deal.
A `Person` is encoded as a `Map` and there's the `withMap` helper for that.

```haskell top
personParser1 :: TaggedValue -> Parser Person
personParser1 =
  withTagged "myapp" "Person" . withMap $ \m ->
    fail "TODO"
```

Let's see if we can fool it with correct label.

```haskell eval
pretty $ parseEither personParser1 (Tagged "myapp" "Person" Nil)
```

Nope, that wouldn't fly. Undefined is not a function and all that. Good.

Maps in EDN are not your ordinary stringy objects with keys that could be any
valid EDN element too, tags and all.
Anyway, there are some common functions to deal with textual keys:

```haskell
mapGetKeyword  :: FromEDN a => Text -> EDNMap -> Parser a
mapGetString   :: FromEDN a => Text -> EDNMap -> Parser a
mapGetSymbol   :: FromEDN a => Text -> EDNMap -> Parser a
mapGetSymbolNS :: FromEDN a => Text -> Text -> EDNMap -> Parser a
```

Note the type variable?
Yes, they even parse the value for you, provided it is present.
Want to pick at them yourself?
No problem, set it to `TaggedValue` by any means you like.

But, it's okay to be a little lazy at times.
Life is too short for writing boilerplate.

```haskell top
personParser :: TaggedValue -> Parser Person
personParser =
  withTagged "myapp" "Person" . withMap $ \m -> Person
    <$> mapGetKeyword "name" m
    <*> mapGetKeyword "title" m
```

Where's our Fred  now? It's right there!

```haskell eval
pretty $ parseEither personParser ednFred
```

Of course it is better to put the parser where it belongs - in a method.

Methods of `FromEDN` are differ a little from its `ToEDN` counterpart.
That's because the class provides parsers while `fromEDN` function runs them.
While `parseEither` always returns Left or Right and requires a parser,
`fromEDN` can operate in any `Monad` and gets the instance-provided
parser automatically.

> Note: Be aware, though, that `fail` can be implemented differently.
> Some times it goes safely with a message, but some "unexpected" instances
> (e.g. `Either String a`) just crash like there's no tomorrow.
> Hence, `parseEither`.

```haskell top
instance FromEDN Person where
  parseEDN =
    withTagged "myapp" "Person" . withMap $ \m -> Person
      <$> mapGetKeyword "name" m
      <*> mapGetKeyword "title" m
```

```haskell eval
pretty (fromEDN ednFred :: Either String Person)
```

And we get `PersonID` and `PersonRow` types for free.

```haskell top
type PersonRow = (PersonID, Person)

newtype PersonID = PersonID Int
  deriving (Eq, Ord, Show, Num, FromEDN)

ednFredRow :: TaggedValue
ednFredRow = NoTag $ mkVec
  [ NoTag (Integer 1)
  , ednFred
  ]
```

```haskell eval
pretty (fromEDN ednFredRow :: Either String PersonRow)
```
