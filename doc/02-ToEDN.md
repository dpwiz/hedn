---
title: Encoding with class
---

Conversion of Haskell values to EDN AST is straight-forward.
There is a `ToEDN` class with two methods: `toEDN` and `toEDNv`.
You implement one of them (preferably `toEDN`, a tagged one) and get another for free.

```haskell top hide
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

import Data.Text (Text)
import Data.EDN
  ( TaggedValue
  , Tagged(..)
  , Value(..)
  , mkMap
  , ToEDN(..)
  , toEDNtagged
  , encodeText
  , renderText
  )
```

Consider now a simple type which you need to shuttle to your friends' ClojureScript application:

```haskell top
data Person = Person
  { pName  :: Text
  , pTitle :: Text
  } deriving (Eq, Show)

theFred :: Person
theFred = Person "Fred" "Mx"
```

It is trivial, then, to write an encoder by hand.

```haskell top
personToEDN :: Person -> TaggedValue
personToEDN Person{..} =
  Tagged "myapp" "Person" $ mkMap
    [ (NoTag $ Keyword "name",  NoTag $ String pName)
    , (NoTag $ Keyword "title", NoTag $ String pTitle)
    ]
```

```haskell eval
renderText $ personToEDN theFred
```

This way you can transform any types to a efficiently-packed
series of characters.
Providing type class instances for your types, however, gives additional
benefits. Such as no-fuss composition with other types and utilizing
a zoo of default instances.

> Obviously `TaggedValue` and `Value` have trivial instances too.
> Trivial as that they give an ability to mix and match direct and
> class-based encoding.

```haskell top
instance ToEDN Person where
  toEDN Person{..} =
    toEDNtagged "myApp" "Person" $ mkMap
      [ (NoTag $ Keyword "name",  toEDN pName)
      , (NoTag $ Keyword "title", toEDN pTitle)
      ]
```

Et voila!

```haskell eval
renderText $ toEDN theFred
```

Using instances instead of manual conversion allows to automatically construct
composite encoders.

```haskell top
newtype PersonID = PersonID Int
  deriving (Eq, Ord, Show, Num, ToEDN)

type PersonRow = (PersonID, Person)

fredRow :: PersonRow
fredRow = (1, theFred)
```

Finally, with one more helper we can now forget about intermediate values:

```haskell eval
encodeText fredRow
```
