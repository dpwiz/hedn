---
title: Home
---

An EDN parsing and encoding library.

Based on [spec] and [hints] published on GitHub.

Source: https://gitlab.com/dpwiz/hedn

Hackage: https://hackage.haskell.org/package/hedn

Stackage: https://www.stackage.org/package/hedn

[spec]: https://github.com/edn-format/edn
[hints]: https://github.com/wagjo/serialization-formats
